use std::fmt;

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum ASN1TagIDs {
    Integer = 0x02,
    BitString = 0x03,
    OctetString = 0x04,
    ObjectIdentifier = 0x06,
    UTF8String = 0x0C,
    Sequence = 0x10,
    UTCTime = 0x17,
    GeneralizedTime = 0x18,
}

#[derive(Copy, Clone)]
pub enum ASN1Classes {
    Universal = 0x00,
    Application = 0x40,
    Context = 0x80,
    Private = 0xC0
}

#[derive(Copy, Clone)]
pub enum ASN1ObjectIdentifiers {
    Sha256WithRSAEncryption,
    Sha384WithRSAEncryption,
}

impl ASN1ObjectIdentifiers {
    pub fn lookup(oid: &ASN1ObjId) -> Option<ASN1ObjectIdentifiers> {
        ASN1ObjectIdentifiers::values().into_iter()
            .find(|&t| t.value() == *oid)
    }

    pub fn values() -> Vec<ASN1ObjectIdentifiers> {
        use ASN1ObjectIdentifiers::*;
        vec![
            Sha256WithRSAEncryption,
            Sha384WithRSAEncryption
        ]
    }

    pub fn value(&self) -> ASN1ObjId {
        match *self {
            ASN1ObjectIdentifiers::Sha256WithRSAEncryption => {
                ASN1ObjId { subidentifiers: vec![1, 2, 840, 113549, 1, 1, 11] }
            },
            ASN1ObjectIdentifiers::Sha384WithRSAEncryption => {
                ASN1ObjId { subidentifiers: vec![1, 2, 840, 113549, 1, 1, 12] }
            },
        }
    }

    pub fn name(&self) -> &str {
        match *self {
            ASN1ObjectIdentifiers::Sha256WithRSAEncryption => "sha256WithRSAEncryption",
            ASN1ObjectIdentifiers::Sha384WithRSAEncryption => "sha384WithRSAEncryption",
        }
    }
}

#[derive(PartialEq, Eq)]
pub struct ASN1ObjId {
    pub subidentifiers: Vec<u64>
}

impl ASN1ObjId {
    pub fn lookup(&self) -> Option<ASN1ObjectIdentifiers> {
        ASN1ObjectIdentifiers::lookup(self)
    }
}

impl fmt::Display for ASN1ObjId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let first = self.subidentifiers.first().unwrap().to_string();
        let concat = |mut s: String, e: &u64| {
            s.push_str(".");
            s.push_str(&e.to_string());
            s
        };
        write!(f, "{}", self.subidentifiers[1..].iter().fold(first, concat))
    }
}

pub struct ASN1UTCTime {
    pub not_before: String,
    pub not_after: String,
}

#[derive(Copy, Clone)]
pub struct ASN1Tag<'a> {
    pub view: &'a[u8],
    pub parent_offset: usize,
    tag: u8,
}

impl<'view> ASN1Tag<'view> {
    // Borrow iterator
    pub fn children(&self, tag: u8) -> ASN1Walker {
        walker(self.view, tag)
    }

    // Move iterator
    pub fn into_children(self, tag: u8) -> ASN1Walker<'view> {
        walker(self.view, tag)
    }

    pub fn oid(&self) -> Option<ASN1ObjId> {
        // Refer to X.690 Section 8.19 for an explanation of this... thing.
        let mut subs: Vec<u64> = match *self.view.first()? {
            x @ 0  ..= 39   => vec![0u64, u64::from(x)],
            x @ 40 ..= 79   => vec![1u64, u64::from(x - 40)],
            x               => vec![2u64, u64::from(x - 80)]
        };

        let walker = |acc: u64, b: &u8| {
            let byte_val: u64 = u64::from(b & 0x7F);
            let mut n: u64 = byte_val.checked_add(acc.checked_mul(128)?)?;
            // If top bit is clear, accumulation is finished
            if b & 0x80 == 0 {
                subs.push(n);
                n = 0;
            }
            Some(n)
        };

        self.view[1..].iter().try_fold(0u64, walker)?;

        Some(ASN1ObjId { subidentifiers: subs })
    }

    pub fn bitstring(&self) -> Option<Box<Vec<u8>>> {
        // First byte is number of unused bits
        let num_unused: u8 = self.view[0];
        if self.view[0] != 0 {
            // TODO: Handle unused bits
            println!("Unused bits in bitstring ({}) are unsupported at this time", num_unused);
            return None;
        }

        let real_slice = &self.view[1..];
        let mut bitstring = Box::new(vec![0u8; real_slice.len()]);
        for (idx, value) in real_slice.iter().enumerate() {
            bitstring[idx] = *value;
        }

        Some(bitstring)
    }

    pub fn string(&self) -> Option<String> {
        match String::from_utf8(self.view.to_vec()) {
            Ok(str) => Some(str),
            Err(_) => None,
        }
    }
}

const LEN_INVALID: u8           = 0xFF;
const LEN_INDEFINITE_FORM: u8   = 0x00;
const LEN_SHORT_FORM: u8        = 0x80;
const LEN_OCTET_VALUE: u8       = 0x7F;

fn len_val(byte: u8) -> usize {
    usize::from(byte & LEN_OCTET_VALUE)
}

fn decode_length(view: &[u8]) -> Option<(usize, usize)> {
    match *(view.first()?) {
        LEN_INDEFINITE_FORM => None,    // Indefinite form, unsupported
        LEN_INVALID => None,            // X.690 suggests this is always invalid for first octet
        // Definite, short form
        x if x & LEN_SHORT_FORM == 0 => Some((len_val(x), 1)),
        // Definite, long form
        y => {
            let num_octets: usize = len_val(y);

            let walker = |acc: usize, b: &u8| {
                usize::from(*b).checked_add(acc.checked_mul(256)?)
            };
            let length: usize = view[1 ..= num_octets].iter().try_fold(0, walker)?;

            Some((length, num_octets + 1))
        },
    }
}

const TAG_CLASS_MASK: u8    = 0xC0;
const TAG_NUM_MASK: u8      = 0x1F;
const TAG_CONSTR: u8        = 0x20;

fn tag_match(tag: u8, test: u8) -> bool {
    ((tag & TAG_CLASS_MASK) == (test & TAG_CLASS_MASK)) &&
        ((tag & TAG_NUM_MASK) == (test & TAG_NUM_MASK))
}

pub struct ASN1Walker<'buf> {
    pub buffer: &'buf[u8],
    pub last_idx: usize,
    pub tag: u8
}

impl<'buf> Iterator for ASN1Walker<'buf> {
    type Item = ASN1Tag<'buf>;

    fn next(&mut self) -> Option<Self::Item> {
        while self.last_idx < self.buffer.len() {
            let view = &self.buffer[self.last_idx..];
            let (tag_len, len_size) = decode_length(&view[1..])?;

            let old_idx = self.last_idx;
            self.last_idx += 1 + tag_len + len_size;

            if tag_match(self.tag, view[0]) {
                let view_offset = old_idx + len_size + 1;
                let view_end = view_offset + tag_len;

                return Some(ASN1Tag {
                    view: &self.buffer[view_offset .. view_end],
                    parent_offset: view_offset,
                    tag: view[0],
                });
            } 
        }

        None
    }
}

pub fn univ_tag(id: ASN1TagIDs) -> u8 {
    (id as u8 & TAG_NUM_MASK) | (ASN1Classes::Universal as u8)
}

pub fn ctx_tag(id: ASN1TagIDs) -> u8 {
    (id as u8 & TAG_NUM_MASK) | (ASN1Classes::Context as u8)
}

pub fn walker<'buf>(contents: &'buf [u8], id: u8) -> ASN1Walker<'buf> {
    ASN1Walker { buffer: contents, tag: id, last_idx: 0 }
}

pub fn tag_walker<'buf>(tag: &'buf ASN1Tag, id: u8) -> ASN1Walker<'buf> {
    ASN1Walker { buffer: tag.view, tag: id, last_idx: 0 }
}
