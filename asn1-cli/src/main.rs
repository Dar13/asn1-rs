use asn1_lib::*;
use asn1_lib::ASN1TagIDs::*;

use base64::decode;

use std::fs::File;
use std::error::Error;
use std::io::{prelude::*, BufReader};
use std::env;

fn decode_file(filename: &str) -> std::io::Result<Vec<u8>> {
    let file = File::open(filename)?;
    let reader = BufReader::new(file);

    let lines = reader.lines()
        // Strip 'BEGIN CERTIFICATE'/'END CERTIFICATE'
        .filter_map(|maybe_line| {
            let line = maybe_line.ok()?;

            if line.contains("BEGIN") || line.contains("END") {
                None
            } else {
                Some(line)
            }
        });

    let mut file_content = String::from("");
    for line in lines {
        file_content.push_str(&line);
    }

    match decode(&file_content) {
        Err(e) => Err(std::io::Error::new(std::io::ErrorKind::InvalidData, e.to_string())),
        Ok(b) => Ok(b)
    }
}

fn hexdump(buf: &[u8]) {
    if buf.len() > 32 {
        let len: usize = buf.len();
        for idx in 0 .. 16 {
            match idx {
                0 | 8               => print!("{:#04x}", buf[idx]),
                7 | 15              => println!(" {:#04x}", buf[idx]),
                1 ..= 6 | 9 ..= 14  => print!(" {:#04x}", buf[idx]),
                _ => ()
            }
        }

        println!("<snipped {} bytes>", len - 32);

        let end_start = len - 17;
        for idx in 0 .. 16 {
            let b_idx = idx + end_start;
            match idx {
                0 | 8               => print!("{:#04x}", buf[b_idx]),
                7 | 15              => println!(" {:#04x}", buf[b_idx]),
                1 ..= 6 | 9 ..= 14  => print!(" {:#04x}", buf[b_idx]),
                _ => ()
            }
        }
    } else {
        let mut need_newline = true;
        for (idx, val) in buf.iter().enumerate() {
            need_newline = match idx {
                0x00 | 0x08 | 0x10 | 0x18     => { print!("{:#04x}", *val); true },
                0x07 | 0x0F | 0x17 | 0x1F     => { println!(" {:#04x}", *val); false },
                0x01 ..= 0x06 | 0x09 ..= 0x0E => { print!(" {:#04x}", *val); true },
                0x11 ..= 0x16 | 0x19 ..= 0x1E => { print!(" {:#04x}", *val); true },
                _ => true,
            }
        }

        if need_newline {
            println!("");
        }
    }
}

fn main() {
    for arg in env::args().skip(1) {
        let contents = decode_file(&arg).unwrap();
        println!("Contents size: {}", contents.len());

        match x509_get_signature_algorithm(&contents) {
            Some(cert_sig_algo) => println!("Certificate signature algorithm: {}", cert_sig_algo.name()),
            None => println!("Unable to retrieve X.509 certificate's signature algorithm!")
        }

        println!("-----");

        match x509_get_validity(&contents) {
            Some(v) => {
                println!("Validity:");
                println!("Not-Before: {}", v.not_before);
                println!("Not-After: {}", v.not_after);
            },
            None => println!("Validity object not found or invalid!")
        }

        println!("-----");

        match x509_get_signature(&contents) {
            Some(sig) => {
                println!("Certificate Signature:");
                hexdump(&sig);
            },
            None => println!("Unable to retrieve X.509 certificate's signature!")
        }
    }
}

fn x509_get_signature_algorithm(buffer: &[u8]) -> Option<ASN1ObjectIdentifiers> {
    walker(&buffer, univ_tag(Sequence)).nth(0)?
        .into_children(univ_tag(Sequence)).nth(1)?
        .into_children(univ_tag(ObjectIdentifier)).nth(0)?
        .oid()?
        .lookup()
}

fn x509_get_validity(buffer: &[u8]) -> Option<ASN1UTCTime> {
    let validity_tag = walker(buffer, univ_tag(Sequence)).nth(0)?
        .into_children(univ_tag(Sequence)).nth(0)?
        .into_children(univ_tag(Sequence)).nth(2)?;

    let validity: ASN1UTCTime = ASN1UTCTime {
        not_after: tag_walker(&validity_tag, univ_tag(UTCTime)).nth(0)?.string()?,
        not_before: tag_walker(&validity_tag, univ_tag(UTCTime)).nth(1)?.string()?,
    };

    Some(validity)
}

fn x509_get_signature(buffer: &[u8]) -> Option<Box<Vec<u8>>> {
    walker(&buffer, univ_tag(Sequence)).nth(0)?
        .into_children(univ_tag(BitString)).nth(0)?
        .bitstring()
}
